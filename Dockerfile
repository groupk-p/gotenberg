FROM thecodingmachine/gotenberg:6

USER root

ADD ./fonts/ /usr/share/fonts/

RUN apt-get install -y \
    ttf-mscorefonts-installer \
    fonts-dejavu \
    fonts-roboto-hinted \
    fonts-lato \
    fonts-firacode \
    fonts-inconsolata \
    fonts-open-sans \
    fonts-liberation \
    fonts-liberation2 \
    fonts-arkpandora \
    fonts-cantarell \
    fonts-courier-prime \
    fonts-ebgaramond \
    fonts-noto \
    fonts-oxygen \
    fonts-quicksand \
    fonts-cardo \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

USER gotenberg
