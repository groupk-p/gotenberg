# Fonts for TicketDesigner

### Fonts file dowload from "windows"
* ___Calibri___
* ___Cambria___
* ___Candara___
* ___Consolas___
* ___Constantia___
* ___Corbel___
* ___Times New Roman___


### Fonts file dowload from "google fonts"
* ___Montserrat___


### Fonts package download from debian package
**With pacakage ttf-mscorefonts-installer package**
* ___Andale Mono___
* ___Arial, Arial Black___
* ___Comic Sans MS___
* ___Courier New___
* ___Georgia___
* ___Impact___
* ___Times New Roman___
* ___Trebuchet___
* ___Verdana___
* ___Webdings___

**With package fonts-dejavu package**
* ___DejaVu___
* ___DejaVu Sans, DejaVu Sans Mono___


**With package fonts-roboto-hinted**
* ___Roboto, Roboto Black, Roboto Light, Roboto Medium, Roboto Thin___
* ___Roboto Condensed, Roboto Condensed Light, Roboto Condensed Medium___


**With package fonts-lato**
* ___Lato, Lato Black, Lato Hairline, Lato Heavy, Lato Light, Lato Medium, Lato Thin, Lato SemiBold___


**With package fonts-firacode**
* ___Fira Code, Fira Code Light, Fira Code Medium, Fira Code Retina___


**With package fonts-inconsolata**
* ___Inconsolata___


**With package fonts-open-sans**
* ___Open Sans, Open Sans ExtraBold, Open Sans Light, Open Sans SemiBold___
* ___Open Sans Condensed, Open Sans Condensed Light___


**With package fonts-liberation / fonts-liberation2**
* ___Liberation Mono___
* ___Liberation Sans___
* ___Liberation Sans Narrow___


**With package fonts-arkpandora**
* ___Aerial, Aerial Mono___
* ___Tymes___
* ___Veranda___


**With package fonts-cantarell**
* ___Cantarell, Cantarell ExtraBold, Cantarell Light, Cantarell Thin___


**With package fonts-courier-prime**
* ___Courier Prime, Courier Prime Code___
* ___Courier Prime Sans___


**With package fonts-ebgaramond**
* ___EB Garamond___


**With package fonts-noto**
* ___Noto Serif, Noto SemiBold___
* ___Noto Sans, Noto Sans Mono___


**With package fonts-oxygen**
* ___Oxygen Sans, Oxygen Mono___


**With package fonts-quicksand**
* ___Quicksand, Quicksand Light, Quicksand Medium___


**With package fonts-cardo**
* ___Cardo___
